//
//  RKRarArchive.h
//  RarKit
//
//  Created by Cory Powers on 4/13/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RKRarArchive;

@protocol RKRarArchiveDelegate <NSObject>
- (void) rarArchive: (RKRarArchive *)anArchive extractFile: (NSString *)aFile extractedBytes: (NSData *)data totalBytesExpected: (long long)totalBytes;
//- (void) rarArchive: (RKRarArchive *)anArchive extractFile: (NSString *)aFile extractedBytes: (long long)extractedBytes totalBytes: (long long)totalBytes;
- (void) rarArchive: (RKRarArchive *)anArchive extractFile:(NSString *)aFile failedWithError: (NSError *)anError;
- (void) rarArchive: (RKRarArchive *)anArchive completedFile:(NSString *)aFile;

@optional
//TODO: Implement extracting all files
- (void) rarArchive: (RKRarArchive *)anArchive extractAllFilesFailedWithError: (NSError *)anError;
- (void) rarArchive: (RKRarArchive *)anArchive didStartExtractingFile: (NSString *)aFile withSize: (long long)totalBytes;
- (void) rarArchive: (RKRarArchive *)anArchive completedExtractingAllFiles: (NSArray *)filesExtracted;
@end

@interface RKRarArchive : NSObject

@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, weak) id<RKRarArchiveDelegate> delegate;

- (id) initWithArchiveFile: (NSString *)aPath;
- (id) initWithArchiveFile: (NSString *)aPath andPassword: (NSString *)aPassword;

- (NSArray *)listOutputFiles;
- (NSArray *)listArchiveFiles;
- (void) extractFile:(NSString *)aFile;
- (void) extractFile:(NSString *)aFile withDelegate: (id <RKRarArchiveDelegate>)aDelegate;
- (void) extractAllFilesToDirectory:(NSString *)aPath;
- (void) extractAllFilesToDirectory:(NSString *)aPath withDelegate: (id <RKRarArchiveDelegate>)aDelegate;
@end
