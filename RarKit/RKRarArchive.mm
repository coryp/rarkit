//
//  RKRarArchive.m
//  RarKit
//
//  Created by Cory Powers on 4/13/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import "RKRarArchive.h"
#import "raros.hpp"
#import "dll.hpp"

@interface RKRarArchive ()
@property (nonatomic, assign) HANDLE rarHandle;
@property (nonatomic, assign) struct RARHeaderDataEx *header;
@property (nonatomic, assign) struct RAROpenArchiveDataEx *flags;
@property (nonatomic, assign) long long extractTotalBytes;
@property (nonatomic, assign) long long extractCurrentBytes;
@property (nonatomic, strong) NSMutableData *fileData;
@property (nonatomic, strong) NSString *extractFile;
@property (nonatomic, strong) NSArray *filesToExtract;
@property (nonatomic, strong) NSString *extractPath;
@property (assign, getter = isExtracting) BOOL extracting;
@property (strong) NSLock *extractLock;

- (BOOL) openFileWithMode: (NSInteger)aMode;
- (void) closeFile;
- (void) extractedBytes: (void *)bytes ofLength: (NSUInteger)size;
- (void) notifyDelegateOfExtractError: (NSError *)anError;
- (void) notifyDelegateOfExtractComplete;
@end

@implementation RKRarArchive

#pragma mark - Initialization
- (id)initWithArchiveFile:(NSString *)aPath {
	return [self initWithArchiveFile:aPath andPassword:nil];
}

- (id)initWithArchiveFile:(NSString *)aPath andPassword:(NSString *)aPassword {
	self = [super init];
	if (self) {
		self.filePath = aPath;
		self.password = aPassword;
		self.extractLock = [[NSLock alloc] init];
	}
	
	return self;
}

#pragma mark - Rar callbacks
int CALLBACK CallbackProc(UINT msg, LPARAM UserData, long P1, long P2) {

	RKRarArchive *archivePointer = (__bridge RKRarArchive *)((void *)UserData);
	switch(msg) {
			
		case UCM_CHANGEVOLUME:
			break;
		case UCM_PROCESSDATA:
			[archivePointer extractedBytes:(void *)P1 ofLength:P2];
			break;
		case UCM_NEEDPASSWORD:
			//TODO: Set the password
			break;
	}

	return(0);
}

#pragma mark - Internal methods
-(BOOL)openFileWithMode:(NSInteger)aMode {
	self.header = new RARHeaderDataEx;
    bzero(self.header, sizeof(RARHeaderDataEx));
	self.flags = new RAROpenArchiveDataEx;
    bzero(self.flags, sizeof(RAROpenArchiveDataEx));
	
	const char *filePathCString = (const char *) [self.filePath UTF8String];
	self.flags->ArcName = new char[strlen(filePathCString) + 1];
	strcpy(self.flags->ArcName, filePathCString);
	self.flags->OpenMode = aMode;
	
	self.rarHandle = RAROpenArchiveEx(self.flags);
	if (self.rarHandle == 0 || self.flags->OpenResult != 0) {
        [self closeFile];
		return NO;
    }
	
    if(self.password != nil) {
        char *passwordCString = (char *) [self.password UTF8String];
        RARSetPassword(self.rarHandle, passwordCString);
    }
    
	return YES;
	
}

-(void)closeFile {
	if (self.rarHandle){
		RARCloseArchive(self.rarHandle);
	}
    self.rarHandle = 0;
    
    if (self.flags){
        delete self.flags->ArcName;
	}
	delete self.flags, self.flags = 0;
    delete self.header, self.header = 0;	
}

- (void) extractedBytes: (void *)bytes ofLength: (NSUInteger)size{
	self.extractCurrentBytes += size;
	NSData *data = [NSData dataWithBytes:bytes length:size];
	[self.delegate rarArchive:self extractFile:self.extractFile extractedBytes:data totalBytesExpected:self.extractTotalBytes];
}

#pragma mark - Delegate notification helper methods
- (void) notifyDelegateOfExtractError: (NSError *)anError {
	
	[self.extractLock lock];
	self.extracting = NO;
	[self.extractLock unlock];

	[self.delegate rarArchive:self extractFile:self.extractFile failedWithError:anError];
	
}

- (void) notifyDelegateOfExtractComplete {
	[self.extractLock lock];
	self.extracting = NO;
	[self.extractLock unlock];

	[self.delegate rarArchive:self completedFile:self.extractFile];
}

#pragma mark - Archive actions
-(NSArray *)listOutputFiles {
	int RHCode = 0, PFCode = 0;
	
	if (![self openFileWithMode:RAR_OM_LIST_INCSPLIT]) {
		return nil;
	}
	
	NSMutableArray *files = [NSMutableArray array];
	while ((RHCode = RARReadHeaderEx(self.rarHandle, self.header)) == 0) {
		NSString *outputFile = [NSString stringWithCString:self.header->FileName encoding:NSASCIIStringEncoding];
		if (![files containsObject:outputFile]) {
			[files addObject:outputFile];			
		}
		
		if ((PFCode = RARProcessFile(self.rarHandle, RAR_SKIP, NULL, NULL)) != 0) {
			[self closeFile];
			return nil;
		}
	}
	
	[self closeFile];
	return files;
}

-(NSArray *)listArchiveFiles {
	int RHCode = 0, PFCode = 0;
	
	if (![self openFileWithMode:RAR_OM_LIST_INCSPLIT]) {
		return nil;
	}
	
	NSMutableArray *files = [NSMutableArray array];
	while ((RHCode = RARReadHeaderEx(self.rarHandle, self.header)) == 0) {
		NSString *archiveFile = [NSString stringWithCString:self.header->ArcName encoding:NSASCIIStringEncoding];
		if (![files containsObject:archiveFile]) {
			[files addObject:archiveFile];
		}
		
		if ((PFCode = RARProcessFile(self.rarHandle, RAR_SKIP, NULL, NULL)) != 0) {
			[self closeFile];
			return nil;
		}
	}
	
	[self closeFile];
	return files;
}

- (void) extractFile:(NSString *)aFile withDelegate: (id <RKRarArchiveDelegate>)aDelegate {
	self.delegate = aDelegate;
	[self extractFile:aFile];
}

-(void) extractFile:(NSString *)aFile {
	int RHCode = 0, PFCode = 0;
	
	if (!self.delegate) {
		// No delegate then no one will receive the data so forget it
		return;
	}

	// Ensure only one extract running at a time
	[self.extractLock lock];
	if (self.isExtracting) {
		[self.extractLock unlock];
		return;
	}
	
	self.extracting = YES;
	[self.extractLock unlock];
	
	self.extractFile = aFile;
	
	NSError *error;
	NSMutableDictionary* errorDetails = [NSMutableDictionary dictionary];
	if(![self openFileWithMode:RAR_OM_EXTRACT]){
		[errorDetails setValue:@"Could not open archive file." forKey:NSLocalizedDescriptionKey];
		error = [NSError errorWithDomain:@"RarKit" code:100 userInfo:errorDetails];
		[self notifyDelegateOfExtractError:error];
		return;
	}
	

	size_t length = 0;
	while ((RHCode = RARReadHeaderEx(self.rarHandle, self.header)) == 0) {
		NSString *outputFileName = [NSString stringWithCString:self.header->FileName encoding:NSASCIIStringEncoding];
		
		if ([outputFileName isEqualToString:aFile]) {
			length = self.header->UnpSize;
			break;
		} else {
			if ((PFCode = RARProcessFile(self.rarHandle, RAR_SKIP, NULL, NULL)) != 0) {
				[errorDetails setValue:@"Error looking for file in archive." forKey:NSLocalizedDescriptionKey];
				error = [NSError errorWithDomain:@"RarKit" code:PFCode userInfo:errorDetails];
				[self notifyDelegateOfExtractError:error];
				[self closeFile];
				return;
			}
		}
	}
	
	if (length == 0) { // archived file not found
		[errorDetails setValue:@"Could not find file in archive." forKey:NSLocalizedDescriptionKey];
		error = [NSError errorWithDomain:@"RarKit" code:101 userInfo:errorDetails];
		[self notifyDelegateOfExtractError:error];
		[self closeFile];
		return nil;
	}
		
	// Put our self in a var accessible by the call back
	
	self.extractTotalBytes = length;
	self.extractCurrentBytes = 0;
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		// Pass ourself as the user param to the call back to support multiple extractions in other RKRarArchive instances
		RARSetCallback(self.rarHandle, CallbackProc, (LPARAM)self);

		int processReturnCode = RARProcessFile(self.rarHandle, RAR_TEST, NULL, NULL);
		if (processReturnCode == 0) {
			[self notifyDelegateOfExtractComplete];
		}else{
			NSError *extractError;
			NSMutableDictionary* extractErrorDetails = [NSMutableDictionary dictionary];
			if(processReturnCode == ERAR_MISSING_PASSWORD) {
				[extractErrorDetails setValue:@"Archive is password protected." forKey:NSLocalizedDescriptionKey];
				extractError = [NSError errorWithDomain:@"RarKit" code:ERAR_MISSING_PASSWORD userInfo:extractErrorDetails];
			}else if (processReturnCode == ERAR_BAD_ARCHIVE){
				[extractErrorDetails setValue:@"Archive is not valid." forKey:NSLocalizedDescriptionKey];
				extractError = [NSError errorWithDomain:@"RarKit" code:ERAR_BAD_ARCHIVE userInfo:extractErrorDetails];
			}else if (processReturnCode == ERAR_UNKNOWN_FORMAT){
				[extractErrorDetails setValue:@"Unknown archive format." forKey:NSLocalizedDescriptionKey];
				extractError = [NSError errorWithDomain:@"RarKit" code:ERAR_UNKNOWN_FORMAT userInfo:extractErrorDetails];
			}else{
				[extractErrorDetails setValue:@"Unknown rar error has occured." forKey:NSLocalizedDescriptionKey];
				extractError = [NSError errorWithDomain:@"RarKit" code:0 userInfo:extractErrorDetails];
			}
			
			[self notifyDelegateOfExtractError:extractError];
		}
		
		[self closeFile];
	});
}

- (void) extractAllFilesToDirectory:(NSString *)aPath withDelegate: (id <RKRarArchiveDelegate>)aDelegate {
	self.delegate = aDelegate;
	[self extractAllFilesToDirectory:aPath];
}

- (void) extractAllFilesToDirectory:(NSString *)aPath {
	self.extractPath = aPath;
	
	//TODO: Implement extracting all files
}


@end
